import { customAlphabet } from 'nanoid';
import jwt from 'jsonwebtoken';

import { JWTConfig, CustomPlayerType, TestPlayer, CustomPlayer } from '@/types';
import { getConfig } from './config';

const ALPHABET =
  '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
const ID_SUFFIX_SIZE = 20;
const nanoid = customAlphabet(ALPHABET, ID_SUFFIX_SIZE);

export const makeId = (prefix = 'player') => {
  return `${prefix}_${nanoid()}`;
};

export const DATETIME_REGEX = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;

export const getCustomPlayer = <T extends CustomPlayerType>(
  type: T,
): CustomPlayer<T> => {
  const jwtConfig = getConfig().params.jwt;
  const player = getConfig().customPlayers?.[type] as CustomPlayer<T>;

  if (!player) {
    throw new Error(
      `Player of type "${type}" not found in config. Generate proper user and add it to customPlayers section in config.yaml`,
    );
  }

  return {
    id: player.id,
    token: jwtConfig ? generateJWT(player.id, jwtConfig) : player.token,
    currency: player.currency,
    payload: player.payload,
  };
};

export const getPlayers = (count: number): Array<TestPlayer> => {
  const jwtConfig = getConfig().params.jwt;
  return getConfig()
    .players.slice(0, count)
    .map(p => {
      p.token = jwtConfig ? generateJWT(p.id, jwtConfig) : p.token;
      p.id = String(p.id);
      return p;
    });
};

export const generateJWT = (playerId: string, config: JWTConfig) => {
  const payload = {
    partnerId: config.partnerId,
    partnerPlayerId: playerId,
  };
  return jwt.sign(payload, config.secret, {
    expiresIn: config.expiresIn,
  });
};

export const dateOffsetSec = (date: Date, offsetSec: number) => {
  return new Date(date.getTime() + offsetSec * 1000);
};

export const multiplyAmountByFactor = (betAmount: number, factor: number) => {
  return Math.floor((betAmount * factor) / 100);
};
