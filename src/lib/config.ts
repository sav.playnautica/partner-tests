import fs from 'fs';
import path from 'path';
import yaml from 'yaml';

import { TestConfig, TestParams } from '@/types';

export const configPath = path.resolve(__dirname, '../../config/config.yaml');

let cachedConfig: TestConfig | undefined;

export const getConfig = (): TestConfig => {
  if (!cachedConfig) {
    cachedConfig = yaml.parse(fs.readFileSync(configPath, 'utf-8'));
  }
  return cachedConfig as TestConfig;
};

export const getTestParams = (): TestParams => {
  return getConfig().params;
};
