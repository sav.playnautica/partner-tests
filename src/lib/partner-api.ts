import _ from 'lodash';
import supertest from 'supertest';
import debug from 'debug';

import { getHmac } from './crypto';
import {
  CreateTestPlayerAdminResponce,
  BetConfig,
  BetRequest,
  GameType,
  MetaBet,
  PartnerAPIVersions,
  PayoutRequest,
  PayoutSource,
  RevokeBetRequest,
  TestPlayer,
} from '@/types';
import { makeId } from './utils';
import { ServerResponse } from 'http';

const requestDebug = debug('http:request');
const responseDebug = debug('http:response');

const debugResponse =
  (method: string, url: string) => (res: supertest.Response) => {
    // Extracting statusMessage from internal NodeJS.Response object
    const httpResponse = (res as any)['res'] as ServerResponse;
    if (responseDebug.enabled) {
      responseDebug(
        `${method} ${url} ${res.statusCode} ${httpResponse.statusMessage}`,
        JSON.stringify(
          {
            headers: res.headers,
            body: res.body,
          },
          null,
          2,
        ),
      );
    }
  };

interface PartnerApiParams {
  partnerId: string;
  apiVersion: PartnerAPIVersions;
  serverUrl: string;
  signatureSecret: string;
  currencyCode: string;
}

export const BET_PARAMS = {
  [GameType.Dices]: {
    num_1: {
      gameType: GameType.Dices,
      slotType: 'num_1',
      slotFactor: 3420, // 1 of 36
      slotValues: ['1_1'],
    },
    range_1_18: {
      gameType: GameType.Dices,
      slotType: 'range_1_18',
      slotFactor: 190,
      slotValues: [
        '1_1',
        '1_2',
        '1_3',
        '1_4',
        '1_5',
        '1_6',
        '2_1',
        '2_2',
        '2_3',
        '2_4',
        '2_5',
        '2_6',
        '3_1',
        '3_2',
        '3_3',
        '3_4',
        '3_5',
        '3_6',
      ],
    },
  },
};

const PUBLIC_ROUND_ID = 'A-077 (1234567)';
const MATCH_ID = makeId('spm');
const MATCH_NAME = 'Иванов - Петров';
const TOURNAMENT_ID = 'spt_20230001';
const TOURNAMENT_NAME =
  'Предварительный турнир(ежедневная серия матчей) «Осенний турнир»';
const MATCH_START_AT = new Date();

type FreebetInput = {
  id?: string;
  amount: number;
  expiresAt: Date;
};

type AdminGeneratePlayerOpts = {
  isTest?: boolean;
  freebets?: Array<FreebetInput>;
};

const getBetMetaParams = (gameType: string, betConfig: BetConfig): MetaBet => {
  if (betConfig.meta) {
    return betConfig.meta;
  }
  if (gameType === GameType.Dices) {
    return BET_PARAMS[gameType].num_1;
  }
  throw new Error(`Unexpected gameType ${gameType}`);
};

export class PartnerApi {
  private _serverUrl: string;
  private _signatureSecret: string;
  private _request: supertest.SuperTest<supertest.Test>;
  private _currencyCode: string;
  private _apiVersion: PartnerAPIVersions;
  private _partnerId: string;

  constructor(params: PartnerApiParams) {
    this._serverUrl = params.serverUrl;
    this._partnerId = params.partnerId;
    this._signatureSecret = params.signatureSecret;
    this._request = supertest(this._serverUrl);
    this._currencyCode = params.currencyCode;
    this._apiVersion = params.apiVersion;
  }

  private fullUrl(url: string, qs?: Record<string, string>) {
    let fullUrl = `${this._serverUrl}${url}`;
    if (qs) {
      fullUrl += `?${_.map(qs, (v, k) => `${k}=${v}`).join('&')}`;
    }
    return fullUrl;
  }

  private post<T extends Object>(url: string, body: T, token?: string) {
    const timestamp = Date.now();
    const bodyWithTimestamp = { ...body, timestamp };
    const signature = getHmac(
      this._signatureSecret,
      JSON.stringify(bodyWithTimestamp),
    );

    const headers = {
      'X-PL-Signature': signature,
      'X-PL-Partner-Id': this._partnerId,
      'X-PL-Partner-Request-Id': makeId('preq'),
      ...(token ? { Authorization: `Bearer ${token}` } : {}),
    };

    const query = { apiVersion: this._apiVersion };
    const fullUrl = this.fullUrl(url, query);

    if (requestDebug.enabled) {
      // Depth is broken for this case, so we instead stringify it manually
      requestDebug(
        `POST ${fullUrl}`,
        JSON.stringify({ headers, body: bodyWithTimestamp }, null, 2),
      );
    }
    return this._request
      .post(url)
      .query(query)
      .set(headers)
      .send(bodyWithTimestamp)
      .on('response', debugResponse('POST', fullUrl));
  }

  getPlayerBalance(token: string, playerId: string) {
    const url = '/balance';
    const query = {
      playerId,
      apiVersion: this._apiVersion,
      gameType: GameType.Dices,
    };
    const fullUrl = this.fullUrl(url, query);
    const headers = {
      Authorization: `Bearer ${token}`,
      'X-PL-Partner-Id': this._partnerId,
      'X-PL-Partner-Request-Id': makeId('preq'),
    };
    if (requestDebug.enabled) {
      // Depth is broken for this case, so we instead stringify it manually
      requestDebug(`GET ${fullUrl}`, JSON.stringify({ headers }, null, 2));
    }
    return this._request
      .get(url)
      .set(headers)
      .set('meta', 'METHOD getPlayerBalance')
      .query(query)
      .on('response', debugResponse('GET', fullUrl));
  }

  createBet(token: string, betRequest: BetRequest) {
    _.forEach(betRequest.bets, (betConfig, betId) => {
      // We don't expect meta to be used for calculations on partner's side,
      // it's rather used for presentation. So we simply fill is with some
      // dummy values.
      const meta = getBetMetaParams(betRequest.gameType, betConfig);
      betRequest.bets[betId] = {
        meta,
        ...meta,
        ...betConfig,
      };
    });

    return this.post(
      '/bet',
      {
        ...betRequest,
        roundPublicId: PUBLIC_ROUND_ID,
        matchId: MATCH_ID,
        matchName: MATCH_NAME,
        matchStartAt: MATCH_START_AT,
        tournamentId: TOURNAMENT_ID,
        tournamentName: TOURNAMENT_NAME,
      },
      token,
    );
  }

  makePayout(payout: PayoutRequest) {
    let roundResult: string | undefined = undefined;
    if (payout.payoutSource === PayoutSource.RoundFinished) {
      roundResult = '4_4';
    } else if (payout.payoutSource === PayoutSource.RoundResultChanged) {
      roundResult = '4_5';
    }

    payout.meta = {
      gameType: payout.gameType,
      roundResult,
    };

    return this.post('/payout', {
      ...payout,
      roundResult,
      roundPublicId: PUBLIC_ROUND_ID,
      matchId: MATCH_ID,
      matchName: MATCH_NAME,
      matchStartAt: MATCH_START_AT,
      tournamentId: TOURNAMENT_ID,
      tournamentName: TOURNAMENT_NAME,
    });
  }

  revokeBets(betsToRevoke: RevokeBetRequest) {
    return this.post('/bet/revoke', {
      ...betsToRevoke,
      roundPublicId: PUBLIC_ROUND_ID,
      matchId: MATCH_ID,
      matchName: MATCH_NAME,
      matchStartAt: MATCH_START_AT,
      tournamentId: TOURNAMENT_ID,
      tournamentName: TOURNAMENT_NAME,
    });
  }

  getHealth() {
    const url = '/health';
    const query = { apiVersion: this._apiVersion };
    const fullUrl = this.fullUrl(url, query);
    const headers = {
      'X-PL-Partner-Id': this._partnerId,
      'X-PL-Partner-Request-Id': makeId('preq'),
    };

    if (requestDebug.enabled) {
      // Depth is broken for this case, so we instead stringify it manually
      requestDebug(`GET ${fullUrl}`, JSON.stringify({ headers }, null, 2));
    }
    return this._request
      .get(url)
      .set(headers)
      .on('response', debugResponse('GET', fullUrl));
  }

  async adminCreatePlayers(
    idFormat: string,
    tokenFormat: string,
    balance: number,
    idRange: { from: number; to: number },
  ): Promise<Array<TestPlayer>> {
    await this._request
      .post('/admin/player/batch-create')
      .set('Authorization', `Bearer ${process.env.PLAYNAUTICA_API_TOKEN}`)
      .send({
        idFormat,
        tokenFormat,
        balance,
        idRange,
      })
      .expect(200);

    return _.range(idRange.from, idRange.to + 1).map(id => ({
      id: idFormat.replace('#', `${id}`),
      token: tokenFormat.replace('#', `${id}`),
      currency: this._currencyCode,
    }));
  }

  async adminGeneratePlayer(
    opts: AdminGeneratePlayerOpts,
  ): Promise<CreateTestPlayerAdminResponce> {
    const playerId = makeId('player');
    const token = makeId('token');

    const res = await this._request
      .post('/admin/player')
      .set('Authorization', `Bearer ${process.env.PLAYNAUTICA_API_TOKEN}`)
      .send({
        playerId,
        token,
        currency: this._currencyCode,
        balance: 1000,
        isTest: opts.isTest ?? false,
        freebets: opts.freebets ?? [],
      })
      .expect(200);

    const { id, balance, currency, isTest, isBanned, freebets } = res.body.data;

    return {
      id,
      balance,
      token,
      currency,
      isTest,
      isBanned,
      freebets,
    };
  }

  async adminBanPlayer(player: TestPlayer): Promise<void> {
    await this._request
      .post('/admin/player/ban')
      .set('Authorization', `Bearer ${process.env.PLAYNAUTICA_API_TOKEN}`)
      .send({ playerId: player.id, token: player.token })
      .expect(200);
  }
}
