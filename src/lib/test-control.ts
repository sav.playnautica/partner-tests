const isJest = global.it !== undefined && global.describe !== undefined;

export class SkipReason<T extends Object> {
  constructor(public failedConditions: Partial<T>) {}

  appendToTestName(name: number | string | Function | jest.FunctionLike) {
    const skipReasons = Object.entries(this.failedConditions)
      .filter(([, value]) => value !== undefined)
      .map(([key, value]) => `${key} = ${value}`)
      .join(', ');

    // Some manual colouring (╯°□°）╯︵ ┻━┻
    return `\x1b[33m[SKIP REASON: ${skipReasons}]\x1b[0m ${name} `;
  }
}

export type CheckTestCondition<T extends Object> = (
  condition: T,
) => undefined | SkipReason<T>;

type ItWithConditionFn<T extends Object> = (
  name: string,
  condition: T,
  fn?: jest.ProvidesCallback,
  timeout?: number,
) => void;

type ItFn = (
  name: string,
  fn?: jest.ProvidesCallback,
  timeout?: number,
) => void;

export type ItWithCondition<T extends Object> = ItWithConditionFn<T> & {
  bindCondition(condition: T): ItFn;
};

export const wrapIt = <T extends Object>(
  jestIt: jest.It,
  check: CheckTestCondition<T>,
): ItWithCondition<T> => {
  const fn = (
    name: string,
    condition: T,
    fn?: jest.ProvidesCallback,
    timeout?: number,
  ) => {
    const skipReason = check(condition);
    return !skipReason
      ? jestIt(name, fn, timeout)
      : jestIt.skip(skipReason.appendToTestName(name), fn, timeout);
  };
  fn.bindCondition = (condition: T) => {
    return (name: string, fn?: jest.ProvidesCallback, timeout?: number) => {
      const skipReason = check(condition);
      return !skipReason
        ? jestIt(name, fn as jest.EmptyFunction, timeout)
        : jestIt.skip(
            skipReason.appendToTestName(name),
            fn as jest.EmptyFunction,
            timeout,
          );
    };
  };
  return fn;
};

export const errorIt = () => {
  throw new Error(
    'global.it is not defined, use this method only within jest tests',
  );
};
errorIt.bindCondition = errorIt;

type DescribeWithConditionFn<T extends Object> = (
  name: number | string | Function | jest.FunctionLike,
  condition: T,
  fn: jest.EmptyFunction,
) => void;
type DescribeFn = (
  name: number | string | Function | jest.FunctionLike,
  fn: jest.EmptyFunction,
) => void;

export type DescribeWithCondition<T extends Object> =
  DescribeWithConditionFn<T> & { bindCondition(condition: T): DescribeFn };

export const wrapDescribe = <T extends Object>(
  jestDescribe: jest.Describe,
  check: CheckTestCondition<T>,
): DescribeWithCondition<T> => {
  const fn = (
    name: number | string | Function | jest.FunctionLike,
    condition: T,
    fn?: jest.EmptyFunction,
  ) => {
    const skipReason = check(condition);
    return !skipReason
      ? jestDescribe(name, fn as jest.EmptyFunction)
      : jestDescribe.skip(
          skipReason.appendToTestName(name),
          fn as jest.EmptyFunction,
        );
  };
  fn.bindCondition = (condition: T) => {
    return (
      name: number | string | Function | jest.FunctionLike,
      fn?: jest.EmptyFunction,
    ) => {
      const skipReason = check(condition);
      return !skipReason
        ? jestDescribe(name, fn as jest.EmptyFunction)
        : jestDescribe.skip(
            skipReason.appendToTestName(name),
            fn as jest.EmptyFunction,
          );
    };
  };
  return fn;
};

export const errorDescribe = () => {
  throw new Error(
    'global.describe is not defined, use this method only within jest tests',
  );
};
errorDescribe.bindCondition = errorDescribe;

export const wrapHelpers = <T extends Object>(check: CheckTestCondition<T>) => {
  if (!isJest) {
    return {
      itIf: errorIt as ItWithCondition<T>,
      describeIf: errorDescribe as DescribeWithCondition<T>,
    };
  }
  return {
    itIf: wrapIt(global.it, check),
    describeIf: wrapDescribe(global.describe, check),
  };
};
