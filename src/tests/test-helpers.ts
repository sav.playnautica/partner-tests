import { PartnerAPIVersions, TestConfig, CustomPlayerType } from '@/types';
import { getConfig, getTestParams } from '@/lib/config';
import { SkipReason, wrapHelpers } from '@/lib/test-control';
import _ from 'lodash';

export type PartnerTestCondition = {
  maxVersion?: PartnerAPIVersions;
  minVersion?: PartnerAPIVersions;
  customPlayerType?: CustomPlayerType;
};

export const checkTestConfig = (testConfig: TestConfig) => {
  const { apiVersion, apiVersionIncludeTests } = testConfig.params;
  const versions = [apiVersion].concat(apiVersionIncludeTests || []);

  return (condition: PartnerTestCondition) => {
    const { maxVersion, minVersion, customPlayerType } = condition;
    const failedConditions: Partial<PartnerTestCondition> = {};

    const hasMatchingVersion = versions.some(version => {
      if (maxVersion && version > maxVersion) {
        return false;
      }

      if (minVersion !== undefined && version < minVersion) {
        return false;
      }

      return true;
    });

    if (!hasMatchingVersion) {
      Object.assign(failedConditions, { maxVersion, minVersion });
    }

    if (customPlayerType && !testConfig.customPlayers?.[customPlayerType]) {
      Object.assign(failedConditions, { customPlayerType });
    }

    return _.isEmpty(failedConditions)
      ? undefined
      : new SkipReason(failedConditions);
  };
};

export const testConfig = getConfig();
export const testParams = getTestParams();
export const { itIf, describeIf } = wrapHelpers<PartnerTestCondition>(
  checkTestConfig(testConfig),
);
