import { getPlayers, makeId } from '@/lib/utils';
import {
  BetOperation,
  GameType,
  PartnerAPIVersions,
  PartnerBetRequestRejectedBetReason,
  PayoutSource,
} from '@/types';
import { PartnerApi } from '@/lib/partner-api';

import { itIf, testParams } from '@/tests/test-helpers';

describe('payouts', () => {
  const partnerApi = new PartnerApi(testParams);
  const gameType = GameType.Dices;

  const [{ id, token }] = getPlayers(1);
  let roundId: string;
  let betId: string;

  const { betAmount, payoutAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    betId = makeId('bet');
  });

  itIf(
    'prevents cancelling bet after payout',
    { maxVersion: PartnerAPIVersions.V1_0 },
    async () => {
      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);

      await partnerApi
        .makePayout({
          gameType,
          roundId,
          payoutId: makeId('payout'),
          payoutSource: PayoutSource.RoundFinished,
          betPayouts: {
            [betId]: {
              betId,
              playerId: id,
              amount: payoutAmount,
              totalAmount: payoutAmount,
            },
          },
        })
        .expect(200);

      const { body } = await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Cancel,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(207);

      expect(body.rejectedBets).toEqual({
        [betId]: {
          reason: PartnerBetRequestRejectedBetReason.BetAlreadyPaidOut,
        },
      });
    },
  );

  itIf(
    'allows cancelling non-payouted bet after payout for round',
    { maxVersion: PartnerAPIVersions.V1_0 },
    async () => {
      const otherBetId = makeId('bet');

      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);

      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [otherBetId]: {
              betId: otherBetId,
              betAmount,
            },
          },
        })
        .expect(200);

      await partnerApi
        .makePayout({
          gameType,
          roundId,
          payoutId: makeId('payout'),
          payoutSource: PayoutSource.RoundFinished,
          betPayouts: {
            // Payout can't be empty, so we create another bet
            [otherBetId]: {
              betId: otherBetId,
              playerId: id,
              amount: payoutAmount,
              totalAmount: payoutAmount,
            },
          },
        })
        .expect(200);

      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Cancel,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);
    },
  );
});
