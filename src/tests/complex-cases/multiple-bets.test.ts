import _ from 'lodash';
import { DATETIME_REGEX, getPlayers, makeId } from '@/lib/utils';
import { BetOperation, GameType, PartnerAPIVersions } from '@/types';
import { PartnerApi } from '@/lib/partner-api';

import { itIf, testParams } from '@/tests/test-helpers';

describe('multiple-bets', () => {
  const partnerApi = new PartnerApi(testParams);
  const gameType = GameType.Dices;

  const [{ id, token }] = getPlayers(1);

  let roundId: string;
  let balance: number;
  const { betAmount, betCount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    const balanceResponse = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    balance = balanceResponse.body.balance;
  });

  it('succesfully makes series of bets', async () => {
    let expectedBalance = balance;
    for (let i = 0; i < betCount; i++) {
      const betId = makeId('bet');
      const makeBetResponse = await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);

      expectedBalance -= betAmount;
      expect(makeBetResponse.body).toEqual({
        operationId: expect.stringMatching(/^.*$/),
        acceptedAt: expect.stringMatching(DATETIME_REGEX),
        balance: expectedBalance,
        rejectedBets: {},
      });
    }

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(expectedBalance);
  });

  it('makes several concurrent bets', async () => {
    await Promise.all(
      _.range(betCount).map(() => {
        const betId = makeId('bet');
        return partnerApi
          .createBet(token, {
            gameType,
            playerId: id,
            roundId,
            betOperation: BetOperation.Make,
            bets: {
              [betId]: {
                betId,
                betAmount,
              },
            },
          })
          .expect(200);
      }),
    );

    const {
      body: { balance: actualBalance },
    } = await partnerApi.getPlayerBalance(token, id).expect(200);
    expect(actualBalance).toEqual(balance - betAmount * betCount);
  });

  itIf(
    'makes several serials bets and cancels',
    { maxVersion: PartnerAPIVersions.V1_0 },
    async () => {
      const betIds = _.range(betCount).map(() => makeId('bet'));
      for (const betId of betIds) {
        await partnerApi
          .createBet(token, {
            gameType,
            playerId: id,
            roundId,
            betOperation: BetOperation.Make,
            bets: {
              [betId]: {
                betId,
                betAmount,
              },
            },
          })
          .expect(200);
      }

      let expectedBalance = balance - betAmount * betCount;
      for (const betId of betIds) {
        const cancelBetResponse = await partnerApi
          .createBet(token, {
            gameType,
            playerId: id,
            roundId,
            betOperation: BetOperation.Cancel,
            bets: {
              [betId]: {
                betId,
                betAmount,
              },
            },
          })
          .expect(200);

        expectedBalance += betAmount;
        expect(cancelBetResponse.body).toEqual({
          operationId: expect.stringMatching(/^.*$/),
          acceptedAt: expect.stringMatching(DATETIME_REGEX),
          balance: expectedBalance,
          rejectedBets: {},
        });
      }

      const actualBalance = await partnerApi
        .getPlayerBalance(token, id)
        .expect(200);
      expect(actualBalance.body.balance).toEqual(balance);
    },
  );

  itIf(
    'makes several concurrent bets and cancels',
    { maxVersion: PartnerAPIVersions.V1_0 },
    async () => {
      await Promise.all(
        _.range(betCount).flatMap(() => {
          const betId = makeId('bet');
          return [
            partnerApi.createBet(token, {
              gameType,
              playerId: id,
              roundId,
              betOperation: BetOperation.Make,
              bets: {
                [betId]: {
                  betId,
                  betAmount,
                },
              },
            }),
            partnerApi.createBet(token, {
              gameType,
              playerId: id,
              roundId,
              betOperation: BetOperation.Cancel,
              bets: {
                [betId]: {
                  betId,
                  betAmount,
                },
              },
            }),
          ];
        }),
      );

      const actualBalance = await partnerApi
        .getPlayerBalance(token, id)
        .expect(200);
      expect(actualBalance.body.balance).toEqual(balance);
    },
  );
});
