import { getPlayers, makeId } from '@/lib/utils';
import { BetOperation, GameType, PartnerAPIVersions } from '@/types';
import { PartnerApi } from '@/lib/partner-api';

import { itIf, testParams } from '@/tests/test-helpers';

describe('bets', () => {
  const partnerApi = new PartnerApi(testParams);
  const gameType = GameType.Dices;

  const [{ id, token }] = getPlayers(1);
  let balance: number;
  let roundId: string;
  let betId: string;

  const { betAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    betId = makeId('bet');
    const balanceResponse = await partnerApi.getPlayerBalance(token, id);
    balance = balanceResponse.body.balance;
  });

  itIf(
    'ignores make bet request if bet was canceled before',
    { maxVersion: PartnerAPIVersions.V1_0 },
    async () => {
      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Cancel,
          bets: {
            [betId]: {
              betId,
              betAmount: balance + 100,
            },
          },
        })
        .expect(200);

      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount: balance + 100,
            },
          },
        })
        .expect(200);

      const actualBalance = await partnerApi
        .getPlayerBalance(token, id)
        .expect(200);
      expect(actualBalance.body.balance).toEqual(balance);
    },
  );

  itIf(
    'ignores multiple make bet requests if bet was canceled before',
    { maxVersion: PartnerAPIVersions.V1_0 },
    async () => {
      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);

      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Cancel,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);

      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);

      const actualBalance = await partnerApi
        .getPlayerBalance(token, id)
        .expect(200);
      expect(actualBalance.body.balance).toEqual(balance);
    },
  );
});
