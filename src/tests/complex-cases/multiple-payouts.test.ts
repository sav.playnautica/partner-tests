import _ from 'lodash';
import {
  BetPayout,
  BetOperation,
  PayoutSource,
  GameType,
  PartnerAPIVersions,
} from '@/types';
import { getPlayers, makeId } from '@/lib/utils';
import { PartnerApi } from '@/lib/partner-api';

import { itIf, testParams } from '@/tests/test-helpers';

// Test can get really long with multiple iterations
// and high concurrency
jest.setTimeout(10000000);

describe('multiple-payouts', () => {
  const partnerApi = new PartnerApi(testParams);
  const gameType = GameType.Dices;

  const [{ id, token }] = getPlayers(1);
  const { betAmount, betCount, payoutAmount } = testParams;

  let balance: number;
  let roundId: string;

  beforeEach(async () => {
    const balanceResponse = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    balance = balanceResponse.body.balance;
    roundId = makeId('round');
  });

  it('makes multiple concurrent payouts', async () => {
    const betIds: Array<string> = [];
    await Promise.all(
      _.range(betCount).map(() => {
        const betId = makeId('bet');
        betIds.push(betId);
        return partnerApi
          .createBet(token, {
            gameType,
            playerId: id,
            roundId,
            betOperation: BetOperation.Make,
            bets: {
              [betId]: {
                betId,
                betAmount,
              },
            },
          })
          .expect(200);
      }),
    );

    await Promise.all(
      betIds.map(betId => {
        const payoutId = makeId('payout');
        return partnerApi
          .makePayout({
            gameType,
            roundId,
            payoutId,
            payoutSource: PayoutSource.RoundFinished,
            betPayouts: {
              [betId]: {
                betId,
                playerId: id,
                amount: payoutAmount,
                totalAmount: payoutAmount,
              },
            },
          })
          .expect(200);
      }),
    );

    const actualBalance = await partnerApi.getPlayerBalance(token, id);
    expect(actualBalance.body.balance).toEqual(
      balance - betCount * betAmount + betCount * payoutAmount,
    );
  });

  it('makes multiple payouts in one request', async () => {
    const betIds: Array<string> = [];

    await Promise.all(
      _.range(betCount).map(() => {
        const betId = makeId('bet');
        betIds.push(betId);
        return partnerApi
          .createBet(token, {
            gameType,
            playerId: id,
            roundId,
            betOperation: BetOperation.Make,
            bets: {
              [betId]: {
                betId,
                betAmount,
              },
            },
          })
          .expect(200);
      }),
    );

    const betPayouts: Record<string, BetPayout> = {};

    betIds.forEach(betId => {
      betPayouts[betId] = {
        betId,
        playerId: id,
        amount: payoutAmount,
        totalAmount: payoutAmount,
      };
    });

    await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundFinished,
        betPayouts,
      })
      .expect(200);

    const actualBalance = await partnerApi.getPlayerBalance(token, id);
    expect(actualBalance.body.balance).toEqual(
      balance - betCount * betAmount + betCount * payoutAmount,
    );
  });

  itIf(
    'makes multiple concurrent bets, cancel bets and payouts',
    { maxVersion: PartnerAPIVersions.V1_0 },
    async () => {
      const { groupsCount, totalPlayers, iterations } = testParams.concurrency;
      const allPlayers = getPlayers(totalPlayers);
      for (let i = 1; i <= iterations; i++) {
        const players = await Promise.all(
          allPlayers.map(async player => {
            const { body } = await partnerApi
              .getPlayerBalance(player.token, player.id)
              .expect(200);
            return {
              ...player,
              balance: body.balance,
            };
          }),
        );

        const groups = _.range(groupsCount).map(() => {
          const makeBetPlayers = _.sampleSize(
            players,
            Math.round(totalPlayers * 0.66),
          ).map(player => ({
            ...player,
            betId: makeId('bet'),
          }));
          const cancelBetPlayers = _.sampleSize(
            makeBetPlayers,
            Math.round(totalPlayers * 0.33),
          );
          return { makeBetPlayers, cancelBetPlayers };
        });

        await Promise.all(
          groups.map(async ({ makeBetPlayers, cancelBetPlayers }) => {
            await Promise.all(
              makeBetPlayers.map(player => {
                return partnerApi
                  .createBet(player.token, {
                    gameType,
                    playerId: player.id,
                    roundId,
                    betOperation: BetOperation.Make,
                    bets: {
                      [player.betId]: {
                        betId: player.betId,
                        betAmount,
                      },
                    },
                  })
                  .expect(200);
              }),
            );

            await Promise.all(
              cancelBetPlayers.map(player => {
                return partnerApi
                  .createBet(player.token, {
                    gameType,
                    playerId: player.id,
                    roundId,
                    betOperation: BetOperation.Cancel,
                    bets: {
                      [player.betId]: {
                        betId: player.betId,
                        betAmount,
                      },
                    },
                  })
                  .expect(200);
              }),
            );

            const notCancelledBetPlayers = _.differenceWith(
              makeBetPlayers,
              cancelBetPlayers,
              _.isEqual,
            );

            return partnerApi.makePayout({
              gameType,
              roundId,
              payoutId: makeId('payout'),
              payoutSource: PayoutSource.RoundFinished,
              betPayouts: notCancelledBetPlayers.reduce((acc, player) => {
                acc[player.betId] = {
                  betId: player.betId,
                  playerId: player.id,
                  amount: payoutAmount,
                  totalAmount: payoutAmount,
                };

                return acc;
              }, {} as { [id: string]: BetPayout }),
            });
          }),
        );

        const playersWithExpectedBalance = groups.reduce(
          (acc, { makeBetPlayers, cancelBetPlayers }) => {
            const notCancelledBetPlayers = _.differenceWith(
              makeBetPlayers,
              cancelBetPlayers,
              _.isEqual,
            );

            notCancelledBetPlayers.forEach(player => {
              if (!acc[player.id]) {
                acc[player.id] = {
                  token: player.token,
                  expectedBalance: player.balance,
                };
              }

              acc[player.id].expectedBalance += payoutAmount - betAmount;
            });

            return acc;
          },
          {} as Record<string, { expectedBalance: number; token: string }>,
        );

        for (const playerId in playersWithExpectedBalance) {
          const actualBalance = await partnerApi
            .getPlayerBalance(
              playersWithExpectedBalance[playerId].token,
              playerId,
            )
            .expect(200);

          expect(actualBalance.body.balance).toEqual(
            playersWithExpectedBalance[playerId].expectedBalance,
          );
        }
      }
    },
  );
});
