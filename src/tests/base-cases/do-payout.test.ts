import {
  DATETIME_REGEX,
  getCustomPlayer,
  getPlayers,
  makeId,
} from '@/lib/utils';
import {
  BetOperation,
  GameType,
  PartnerAPIVersions,
  PartnerPayoutRequestRejectedBetReason,
  PartnerRequestErrorCodes,
  PayoutSource,
  CustomPlayerType,
} from '@/types';
import { PartnerApi } from '@/lib/partner-api';

import { itIf, testParams } from '@/tests/test-helpers';

describe('do-payout', () => {
  const partnerApi = new PartnerApi(testParams);
  const gameType = GameType.Dices;

  const [{ id, token }] = getPlayers(1);
  let startingBalance: number;
  let roundId: string;
  let betId: string;
  let payoutId: string;

  const { betAmount, payoutAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('rnd');
    betId = makeId('bet');
    payoutId = makeId('po');
    const balanceResponse = await partnerApi.getPlayerBalance(token, id);
    startingBalance = balanceResponse.body.balance;
  });

  it('makes payout for existing player with existing bet', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(
      startingBalance - betAmount + payoutAmount,
    );
  });

  itIf(
    'does not makes payout for cancelled bet',
    { maxVersion: PartnerAPIVersions.V1_0 },
    async () => {
      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);

      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Cancel,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);

      const { body } = await partnerApi
        .makePayout({
          gameType,
          roundId,
          payoutId,
          payoutSource: PayoutSource.RoundFinished,
          betPayouts: {
            [betId]: {
              betId,
              playerId: id,
              amount: payoutAmount,
              totalAmount: payoutAmount,
            },
          },
        })
        .expect(207);

      expect(body).toEqual({
        operationId: expect.stringMatching(/^.*$/),
        acceptedAt: expect.stringMatching(DATETIME_REGEX),
        rejectedBets: {
          [betId]: {
            reason: PartnerPayoutRequestRejectedBetReason.BetWasCancelled,
          },
        },
      });

      const actualBalance = await partnerApi
        .getPlayerBalance(token, id)
        .expect(200);
      expect(actualBalance.body.balance).toEqual(startingBalance);
    },
  );

  itIf(
    'rejects payout if player was banned',
    { customPlayerType: CustomPlayerType.BannedPlayer },
    async () => {
      // We create bet with regular player to make sure
      // that rounds actually exists to prevent 404 in payout
      const betId = makeId('bet');
      await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(200);

      const bannedPlayer = getCustomPlayer(CustomPlayerType.BannedPlayer);
      const bannedPlayerBetId = makeId('bet');

      const { body } = await partnerApi
        .makePayout({
          gameType,
          roundId,
          payoutId,
          payoutSource: PayoutSource.RoundFinished,
          betPayouts: {
            // Normal bet, should be accepted just fine
            [betId]: {
              betId,
              playerId: id,
              amount: betAmount,
              totalAmount: betAmount,
            },
            // Banned player bet, should be rejected
            [bannedPlayerBetId]: {
              betId: bannedPlayerBetId,
              playerId: bannedPlayer.id,
              // any amount is ok, not important
              amount: betAmount,
              totalAmount: betAmount,
            },
          },
        })
        .expect(207);

      expect(body).toEqual({
        operationId: expect.stringMatching(/^.*$/),
        acceptedAt: expect.stringMatching(DATETIME_REGEX),
        rejectedBets: {
          [bannedPlayerBetId]: {
            reason: PartnerPayoutRequestRejectedBetReason.PlayerBanned,
          },
        },
      });
    },
  );

  it('does not makes payout if bet does not exists', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    const unknownBet = makeId('bet');
    const { body } = await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [unknownBet]: {
            betId: unknownBet,
            playerId: id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
          [betId]: {
            betId,
            playerId: id,
            amount: 0,
            totalAmount: 0,
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {
        [unknownBet]: {
          reason: PartnerPayoutRequestRejectedBetReason.BetNotFound,
        },
      },
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(startingBalance - betAmount);
  });

  it('does not makes payout if player was not found', async () => {
    await partnerApi.createBet(token, {
      gameType,
      playerId: id,
      roundId,
      betOperation: BetOperation.Make,
      bets: {
        [betId]: {
          betId,
          betAmount,
        },
      },
    });

    const { body } = await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            betId,
            playerId: 'invalid-player-id',
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {
        [betId]: {
          reason: PartnerPayoutRequestRejectedBetReason.PlayerNotFound,
        },
      },
    });
  });

  it('payouts only once if same payout received multiple times', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(
      startingBalance - betAmount + payoutAmount,
    );
  });

  it('can cancel round before any other payout happened', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundCancelled,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: betAmount,
            totalAmount: betAmount,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);

    expect(actualBalance.body.balance).toEqual(startingBalance);
  });

  it('can make multiple payouts for same round for same bet', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundResultChanged,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: -payoutAmount,
            totalAmount: 0,
          },
        },
      })
      .expect(200);

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);

    expect(actualBalance.body.balance).toEqual(startingBalance - betAmount);
  });

  it('may rollback after payout round with new payout with negative amount', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundRolledBack,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: -payoutAmount + betAmount,
            totalAmount: betAmount,
          },
        },
      })
      .expect(200);

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);

    expect(actualBalance.body.balance).toEqual(startingBalance);
  });

  it('throws error if round not found', async () => {
    const unknownGameRoundId = makeId('round');
    const { body } = await partnerApi
      .makePayout({
        gameType,
        roundId: unknownGameRoundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      })
      .expect(404);

    expect(body.errorCode).toEqual(PartnerRequestErrorCodes.RoundNotFound);
  });

  it('throws error on unknown gameType', async () => {
    const invalidGameType = 'invalid-game-type';
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .makePayout({
        gameType: invalidGameType,
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      })
      .expect(400);

    expect(body.errorCode).toEqual(PartnerRequestErrorCodes.UnknownGameType);
  });
});
