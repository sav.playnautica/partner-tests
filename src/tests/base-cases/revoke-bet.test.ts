import {
  DATETIME_REGEX,
  getCustomPlayer,
  getPlayers,
  makeId,
} from '@/lib/utils';
import {
  BetOperation,
  CustomPlayerType,
  GameType,
  PartnerAPIVersions,
  PartnerRequestErrorCodes,
  PartnerRevokeBetRequestRejectedBetReason,
  PayoutSource,
} from '@/types';
import { PartnerApi } from '@/lib/partner-api';

import { itIf, describeIf, testParams } from '@/tests/test-helpers';

describeIf('revoke-bet', { minVersion: PartnerAPIVersions.V1_1 }, () => {
  const partnerApi = new PartnerApi(testParams);
  const gameType = GameType.Dices;

  const [{ id, token }] = getPlayers(1);

  let balance: number;
  let roundId: string;
  let betId: string;
  const { betAmount, payoutAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    betId = makeId('bet');
    const balanceResponse = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    balance = balanceResponse.body.balance;
  });

  it('revoke bet', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .revokeBets({
        gameType,
        roundId,
        bets: {
          [betId]: {
            betId,
            betAmount,
            playerId: id,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('revoke multiple bets at once', async () => {
    const [firstBetId, secondBetId, thirdBetId] = [
      makeId('bet'),
      makeId('bet'),
      makeId('bet'),
    ];
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [firstBetId]: {
            betId: firstBetId,
            betAmount,
          },
          [secondBetId]: {
            betId: secondBetId,
            betAmount,
          },
          [thirdBetId]: {
            betId: thirdBetId,
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .revokeBets({
        gameType,
        roundId,
        bets: {
          [firstBetId]: {
            betId: firstBetId,
            betAmount,
            playerId: id,
          },
          [secondBetId]: {
            betId: secondBetId,
            betAmount,
            playerId: id,
          },
          [thirdBetId]: {
            betId: thirdBetId,
            betAmount,
            playerId: id,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('does not reject revoke bets if bet does not exists', async () => {
    const [firstBetId, secondBetId, thirdBetId] = [
      makeId('bet'),
      makeId('bet'),
      makeId('bet'),
    ];
    const { body } = await partnerApi
      .revokeBets({
        gameType,
        roundId,
        bets: {
          [firstBetId]: {
            betId: firstBetId,
            betAmount,
            playerId: id,
          },
          [secondBetId]: {
            betId: secondBetId,
            betAmount,
            playerId: id,
          },
          [thirdBetId]: {
            betId: thirdBetId,
            betAmount,
            playerId: id,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {},
    });

    const actualBalance = await partnerApi.getPlayerBalance(token, id);
    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('reject revoke bet with wrong betAmount', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .revokeBets({
        gameType,
        roundId,
        bets: {
          [betId]: {
            betId,
            betAmount: betAmount + 100,
            playerId: id,
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {
        [betId]: {
          reason: PartnerRevokeBetRequestRejectedBetReason.BetRejected,
        },
      },
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(actualBalance.body.balance).toEqual(balance - betAmount);
  });

  it('reject revoke bet if player not found', async () => {
    const { body } = await partnerApi
      .revokeBets({
        gameType,
        roundId,
        bets: {
          [betId]: {
            betId,
            betAmount: betAmount,
            playerId: 'wrong_player_id',
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {
        [betId]: {
          reason: PartnerRevokeBetRequestRejectedBetReason.PlayerNotFound,
        },
      },
    });
  });

  it('reject revoke bet if bet already paid out', async () => {
    const payoutId = makeId('payout');
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .makePayout({
        gameType,
        roundId,
        payoutId,
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betId]: {
            betId,
            playerId: id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .revokeBets({
        gameType,
        roundId,
        bets: {
          [betId]: {
            betId,
            betAmount: betAmount,
            playerId: id,
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {
        [betId]: {
          reason: PartnerRevokeBetRequestRejectedBetReason.BetAlreadyPaidOut,
        },
      },
    });
  });

  itIf(
    'reject revoke bet if player was banned',
    { customPlayerType: CustomPlayerType.BannedPlayer },
    async () => {
      const { id } = getCustomPlayer(CustomPlayerType.BannedPlayer);
      const { body } = await partnerApi
        .revokeBets({
          gameType,
          roundId,
          bets: {
            [betId]: {
              betId,
              betAmount,
              playerId: id,
            },
          },
        })
        .expect(207);

      expect(body).toEqual({
        operationId: expect.stringMatching(/^.*$/),
        acceptedAt: expect.stringMatching(DATETIME_REGEX),
        rejectedBets: {
          [betId]: {
            reason: PartnerRevokeBetRequestRejectedBetReason.PlayerBanned,
          },
        },
      });
    },
  );

  it('throws if revoke bet with invalid bet amount', async () => {
    await partnerApi
      .revokeBets({
        gameType,
        roundId,
        bets: {
          [betId]: {
            betId,
            betAmount: 'this-is-not-amount' as unknown as number,
            playerId: id,
          },
        },
      })
      .expect(400);
  });

  it('not changes player balance if making multiple revoke bet operations for the same bet', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    await partnerApi
      .revokeBets({
        gameType,
        roundId,
        bets: {
          [betId]: {
            betId,
            betAmount,
            playerId: id,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .revokeBets({
        gameType,
        roundId,
        bets: {
          [betId]: {
            betId,
            betAmount,
            playerId: id,
          },
        },
      })
      .expect(200);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      rejectedBets: {},
    });

    const actualBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);

    expect(actualBalance.body.balance).toEqual(balance);
  });

  it('throws error on unknown gameType', async () => {
    const unknownGameType = 'unknown-game-type';
    const { body } = await partnerApi
      .revokeBets({
        gameType: unknownGameType,
        roundId,
        bets: {
          [betId]: {
            betId,
            betAmount,
            playerId: id,
          },
        },
      })
      .expect(400);

    expect(body.errorCode).toEqual(PartnerRequestErrorCodes.UnknownGameType);
  });
});
