import {
  BetOperation,
  GameType,
  PartnerAPIVersions,
  PayoutSource,
} from '@/types';
import { getPlayers, makeId } from '@/lib/utils';
import { PartnerApi } from '@/lib/partner-api';

import { itIf, testParams } from '@/tests/test-helpers';

describe('base-round-flow', () => {
  const { betAmount, payoutAmount } = testParams;
  const partnerApi = new PartnerApi(testParams);
  const gameType = GameType.Dices;

  itIf(
    'makes several bets, cancel bets, and then do payout',
    { maxVersion: PartnerAPIVersions.V1_0 },
    async () => {
      const [player] = getPlayers(1);
      const balanceResponse = await partnerApi
        .getPlayerBalance(player.token, player.id)
        .expect(200);
      const balance = balanceResponse.body.balance;

      const roundId = makeId('round');
      const betIds = {
        firstBet: makeId('bet'),
        secondBet: makeId('bet'),
        cancelledBet: makeId('bet'),
      };

      await partnerApi.createBet(player.token, {
        gameType,
        playerId: player.id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betIds.firstBet]: {
            betId: betIds.firstBet,
            betAmount,
          },
        },
      });

      await partnerApi.createBet(player.token, {
        gameType,
        playerId: player.id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betIds.secondBet]: {
            betId: betIds.secondBet,
            betAmount,
          },
        },
      });

      await partnerApi.createBet(player.token, {
        gameType,
        playerId: player.id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betIds.cancelledBet]: {
            betId: betIds.cancelledBet,
            betAmount,
          },
        },
      });

      await partnerApi.createBet(player.token, {
        gameType,
        playerId: player.id,
        roundId,
        betOperation: BetOperation.Cancel,
        bets: {
          [betIds.cancelledBet]: {
            betId: betIds.cancelledBet,
            betAmount,
          },
        },
      });

      await partnerApi.makePayout({
        gameType,
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betIds.secondBet]: {
            betId: betIds.secondBet,
            playerId: player.id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      });

      const { body } = await partnerApi.getPlayerBalance(
        player.token,
        player.id,
      );
      const expectedBalance = balance - 2 * betAmount + payoutAmount;

      expect(body.balance).toEqual(expectedBalance);
    },
  );

  itIf(
    'makes bet, payout and then do revoke',
    { minVersion: PartnerAPIVersions.V1_1 },
    async () => {
      const [player] = getPlayers(1);
      const balanceResponse = await partnerApi
        .getPlayerBalance(player.token, player.id)
        .expect(200);
      const balance = balanceResponse.body.balance;

      const roundId = makeId('round');
      const betIds = {
        firstBet: makeId('bet'),
        secondBet: makeId('bet'),
      };

      await partnerApi.createBet(player.token, {
        gameType,
        playerId: player.id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betIds.firstBet]: {
            betId: betIds.firstBet,
            betAmount,
          },
        },
      });

      await partnerApi.createBet(player.token, {
        gameType,
        playerId: player.id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betIds.secondBet]: {
            betId: betIds.secondBet,
            betAmount,
          },
        },
      });

      await partnerApi.makePayout({
        gameType,
        roundId,
        payoutId: makeId('payout'),
        payoutSource: PayoutSource.RoundFinished,
        betPayouts: {
          [betIds.firstBet]: {
            betId: betIds.firstBet,
            playerId: player.id,
            amount: payoutAmount,
            totalAmount: payoutAmount,
          },
        },
      });

      await partnerApi
        .revokeBets({
          gameType,
          roundId,
          bets: {
            [betIds.secondBet]: {
              betId: betIds.secondBet,
              betAmount,
              playerId: player.id,
            },
          },
        })
        .expect(200);

      const { body } = await partnerApi.getPlayerBalance(
        player.token,
        player.id,
      );
      const expectedBalance = balance - betAmount + payoutAmount;

      expect(body.balance).toEqual(expectedBalance);
    },
  );
});
