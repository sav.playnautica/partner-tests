import {
  BetOperation,
  CustomPlayerType,
  FreebetData,
  FreebetTestType,
  GameType,
  PartnerAPIVersions,
  PartnerBetRequestRejectedBetReason,
  PayoutSource,
} from '@/types';
import { describeIf, testParams } from '../test-helpers';
import { BET_PARAMS, PartnerApi } from '@/lib/partner-api';
import { getCustomPlayer, makeId, multiplyAmountByFactor } from '@/lib/utils';
import _ from 'lodash';

describeIf(
  'freebet',
  {
    customPlayerType: CustomPlayerType.FreebetPlayer,
    minVersion: PartnerAPIVersions.V1_1,
  },
  () => {
    const player = getCustomPlayer(CustomPlayerType.FreebetPlayer);
    const partnerApi = new PartnerApi(testParams);
    const gameType = GameType.Dices;

    let betId: string;
    let roundId: string;
    let freebetsById: Record<string, FreebetData>;
    let playerBalance: number;

    const getFreebetById = (freebetId: string) => {
      const res = freebetsById[freebetId];
      if (!res) {
        throw new Error(
          `Freebet with id ${freebetId} not found for player ${player.id} (already used?)`,
        );
      }
      return res;
    };

    beforeAll(async () => {
      const { body } = await partnerApi
        .getPlayerBalance(player.token, player.id)
        .expect(200);
      const freebets = (body.freebets || []) as Array<FreebetData>;
      freebetsById = _.keyBy(freebets, 'id');
    });

    beforeEach(async () => {
      betId = makeId('bet');
      roundId = makeId('round');
      const { body } = await partnerApi
        .getPlayerBalance(player.token, player.id)
        .expect(200);
      playerBalance = body.balance;
    });

    it('/bet accepts freebet for bet and /payout accepts proper payout', async () => {
      const freebetId =
        player.payload.freebetTypes[FreebetTestType.RegularFreebet];

      const meta = BET_PARAMS[gameType].range_1_18;
      const { slotFactor } = meta;
      const freebetAmount = getFreebetById(freebetId).amount;
      const res = await partnerApi
        .createBet(player.token, {
          gameType,
          playerId: player.id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount: 0,
              freebetId,
              freebetAmount,
              // manually set bet params to have factor in scope explicitly
              meta,
            },
          },
        })
        .expect(200);

      expect(res.body.rejectedBets).toEqual({});

      {
        // No money consumer from balance, only freebet
        const getBalanceRes = await partnerApi
          .getPlayerBalance(player.token, player.id)
          .expect(200);

        expect(getBalanceRes.body.balance).toEqual(playerBalance);
        const foundFreebets = (getBalanceRes.body.freebets ||
          []) as Array<FreebetData>;

        // Freebet must be considered used and removed from list of freebets
        expect(foundFreebets.find(freebet => freebet.id === freebetId)).toEqual(
          undefined,
        );
      }

      const freebetPayoutAmount = multiplyAmountByFactor(
        freebetAmount,
        slotFactor - 100,
      );

      const payoutRes = await partnerApi
        .makePayout({
          gameType,
          roundId,
          payoutId: makeId('payout'),
          payoutSource: PayoutSource.RoundFinished,
          betPayouts: {
            [betId]: {
              betId: betId,
              playerId: player.id,
              amount: freebetPayoutAmount,
              totalAmount: freebetPayoutAmount,
            },
          },
        })
        .expect(200);
      expect(payoutRes.body.rejectedBets).toEqual({});

      {
        const getBalanceRes = await partnerApi
          .getPlayerBalance(player.token, player.id)
          .expect(200);

        // Freebet is paid out succesfully
        expect(getBalanceRes.body.balance).toEqual(
          playerBalance + freebetPayoutAmount,
        );
      }
    });

    it('rejects bet if betting with expired freebet', async () => {
      const freebetId =
        player.payload.freebetTypes[FreebetTestType.ExpiredFreebet];

      const freebetAmount = getFreebetById(freebetId).amount;
      const res = await partnerApi
        .createBet(player.token, {
          gameType,
          playerId: player.id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount: 0,
              freebetId,
              freebetAmount,
            },
          },
        })
        .expect(207);

      expect(res.body.rejectedBets).toEqual({
        [betId]: {
          reason: PartnerBetRequestRejectedBetReason.FreebetExpired,
        },
      });
    });

    it('rejects bet if betting with already used freebet', async () => {
      const freebetId =
        player.payload.freebetTypes[FreebetTestType.AlreadyUsedFreebet];

      const freebetAmount = getFreebetById(freebetId).amount;
      await partnerApi
        .createBet(player.token, {
          gameType,
          playerId: player.id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount: 0,
              freebetId,
              freebetAmount,
            },
          },
        })
        .expect(200);

      const otherBetId = makeId('bet');
      const res = await partnerApi
        .createBet(player.token, {
          gameType,
          playerId: player.id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [otherBetId]: {
              betId: otherBetId,
              betAmount: 0,
              freebetId,
              freebetAmount,
            },
          },
        })
        .expect(207);
      expect(res.body.rejectedBets).toEqual({
        [otherBetId]: {
          reason: PartnerBetRequestRejectedBetReason.FreebetAlreadyUsed,
        },
      });
    });

    it('rejects one of bets if betting with same freebet twice', async () => {
      const freebetId =
        player.payload.freebetTypes[FreebetTestType.ConcurrentlyUsedFreebet];

      const freebetAmount = getFreebetById(freebetId).amount;
      const otherBetId = makeId('bet');
      const res = await partnerApi
        .createBet(player.token, {
          gameType,
          playerId: player.id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount: 0,
              freebetId,
              freebetAmount,
            },
            [otherBetId]: {
              betId: otherBetId,
              betAmount: 0,
              freebetId,
              freebetAmount,
            },
          },
        })
        .expect(207);
      expect(res.body.rejectedBets).toEqual({
        [otherBetId]: {
          reason: PartnerBetRequestRejectedBetReason.FreebetAlreadyUsed,
        },
      });
    });

    it('rejects bet if betting with incorrect freebet amount', async () => {
      const freebetId =
        player.payload.freebetTypes[FreebetTestType.IncorectAmountFreebet];

      const freebetAmount = getFreebetById(freebetId).amount;
      const res = await partnerApi
        .createBet(player.token, {
          gameType,
          playerId: player.id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount: 0,
              freebetId,
              freebetAmount: freebetAmount + 1000,
            },
          },
        })
        .expect(207);

      expect(res.body.rejectedBets).toEqual({
        [betId]: {
          reason: PartnerBetRequestRejectedBetReason.IncorrectFreebetAmount,
        },
      });
    });

    it('allows to revoke bet with freebet', async () => {
      const freebetId =
        player.payload.freebetTypes[FreebetTestType.RevokedFreebet];

      const freebetAmount = getFreebetById(freebetId).amount;
      await partnerApi
        .createBet(player.token, {
          gameType,
          playerId: player.id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount: 0,
              freebetId,
              freebetAmount,
            },
          },
        })
        .expect(200);

      const res = await partnerApi
        .revokeBets({
          gameType,
          roundId,
          bets: {
            [betId]: {
              betId,
              betAmount: 0,
              playerId: player.id,
            },
          },
        })
        .expect(200);

      expect(res.body.rejectedBets).toEqual({});
    });
  },
);
