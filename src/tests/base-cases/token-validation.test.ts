import { getTestParams } from '@/lib/config';
import { getHmac } from '@/lib/crypto';
import { getPlayers, makeId } from '@/lib/utils';
import { BetOperation, GameType, PartnerRequestErrorCodes } from '@/types';
import supertest from 'supertest';

describe('Token validation tests', () => {
  const testParams = getTestParams();
  const request = supertest(testParams.serverUrl);

  describe(`GET /balance`, () => {
    const [{ id }] = getPlayers(1);
    it('throws unauthorized error when token is missing', async () => {
      const { body } = await request
        .get('/balance')
        .query({ playerId: id, apiVersion: testParams.apiVersion })
        .expect(401);

      expect(body.errorCode).toEqual(PartnerRequestErrorCodes.TokenMissing);
    });

    it('throws unauthorized error on incorrect token', async () => {
      const { body } = await request
        .get('/balance')
        .set('Authorization', 'Bearer invalid-token')
        .query({ playerId: id, apiVersion: testParams.apiVersion })
        .expect(403);

      expect(body.errorCode).toEqual(PartnerRequestErrorCodes.IncorrectToken);
    });
  });

  describe('POST /bet', () => {
    const gameType = GameType.Dices;
    const [{ id }] = getPlayers(1);
    const roundId = makeId('round');
    const betId = makeId('bet');
    const { betAmount } = testParams;
    const betMetaData = {
      slotType: 'row_1',
      slotFactor: 200,
      slotValues: ['1', '4', '7', '10'],
      meta: {
        slotType: 'row_1',
        slotFactor: 200,
        slotValues: ['1', '4', '7', '10'],
        gameType,
      },
    };
    const betRequestBody = {
      gameType,
      playerId: id,
      roundId,
      betOperation: BetOperation.Make,
      bets: {
        [betId]: {
          betId,
          betAmount,
          ...betMetaData,
        },
      },
      timestamp: Date.now(),
    };
    const signature = getHmac(
      testParams.signatureSecret,
      JSON.stringify(betRequestBody),
    );

    it('throws unauthorized error when token is missing', async () => {
      const { body } = await request
        .post('/bet')
        .query({ apiVersion: testParams.apiVersion })
        .set('X-PL-Signature', signature)
        .send(betRequestBody)
        .expect(401);

      expect(body.errorCode).toEqual(PartnerRequestErrorCodes.TokenMissing);
    });

    it('throws unauthorized error on incorrect token', async () => {
      const { body } = await request
        .post('/bet')
        .query({ apiVersion: testParams.apiVersion })
        .set('X-PL-Signature', signature)
        .set('Authorization', 'Bearer invalid-token')
        .send(betRequestBody)
        .expect(403);

      expect(body.errorCode).toEqual(PartnerRequestErrorCodes.IncorrectToken);
    });
  });
});
