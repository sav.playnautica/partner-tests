import { getCustomPlayer, getPlayers, DATETIME_REGEX } from '@/lib/utils';
import { PartnerApi } from '@/lib/partner-api';
import {
  CustomPlayerType,
  FreebetTestType,
  PartnerAPIVersions,
  PartnerRequestErrorCodes,
} from '@/types';

import { itIf, testParams } from '@/tests/test-helpers';

describe('get-balance', () => {
  const [{ id, token, currency }] = getPlayers(1);
  const partnerApi = new PartnerApi(testParams);

  it('balance for existing player with valid token', async () => {
    const { body } = await partnerApi.getPlayerBalance(token, id).expect(200);

    expect(body).toEqual(
      expect.objectContaining({
        playerId: id,
        balance: expect.any(Number),
        currency: currency,
      }),
    );
    expect(
      typeof body.isTest === 'boolean' || typeof body.isTest === 'undefined',
    ).toBe(true);
  });

  it('throws if getting balance with invalid token', async () => {
    await partnerApi.getPlayerBalance('invalid-token', id).expect(403);
  });

  it('throws error if player not found', async () => {
    const { body } = await partnerApi
      .getPlayerBalance('invalid-token', 'invalid-player-id')
      .expect(404);

    expect(body.errorCode).toEqual(PartnerRequestErrorCodes.PlayerNotFound);
  });

  itIf(
    'throws error if player banned',
    { customPlayerType: CustomPlayerType.BannedPlayer },
    async () => {
      const { id, token } = getCustomPlayer(CustomPlayerType.BannedPlayer);
      const { body } = await partnerApi.getPlayerBalance(token, id).expect(403);

      expect(body.errorCode).toEqual(PartnerRequestErrorCodes.PlayerBanned);
    },
  );

  itIf(
    'returns isTest flag for testing player',
    { customPlayerType: CustomPlayerType.TestingPlayer },
    async () => {
      const { id, token } = getCustomPlayer(CustomPlayerType.TestingPlayer);
      const { body } = await partnerApi.getPlayerBalance(token, id).expect(200);
      expect(body).toEqual(
        expect.objectContaining({
          playerId: id,
          balance: expect.any(Number),
          currency: currency,
          isTest: true,
        }),
      );
    },
  );

  itIf(
    'returns freebets if player has any',
    {
      customPlayerType: CustomPlayerType.FreebetPlayer,
      minVersion: PartnerAPIVersions.V1_1,
    },
    async () => {
      const { id, token, payload } = getCustomPlayer(
        CustomPlayerType.FreebetPlayer,
      );
      const { body } = await partnerApi.getPlayerBalance(token, id).expect(200);
      expect(body).toEqual(
        expect.objectContaining({
          freebets: expect.arrayContaining([
            {
              id: payload.freebetTypes[FreebetTestType.NewFreebet],
              amount: expect.any(Number),
              expiresAt: expect.stringMatching(DATETIME_REGEX),
            },
          ]),
        }),
      );
    },
  );
});
