import { PartnerAPIVersions, PartnerRequestErrorCodes } from '@/types';
import supertest from 'supertest';

import { describeIf, testParams } from '@/tests/test-helpers';

describe('signature', () => {
  const request = supertest(testParams.serverUrl);
  const playerId = 'player_id';

  [
    { url: '/bet' },
    { url: '/payout' },
    { url: '/bet/revoke', condition: { minVersion: PartnerAPIVersions.V1_1 } },
  ].map(({ url, condition }) => {
    const describe = condition
      ? describeIf.bindCondition(condition)
      : global.describe;
    describe(`POST ${url}`, () => {
      it('return proper error when signature missing', async () => {
        const timestamp = Date.now();
        const { body } = await request
          .post(url)
          .send({ timestamp })
          .expect(403);

        expect(body.errorCode).toEqual(
          PartnerRequestErrorCodes.IncorrectSignature,
        );
      });

      it('returns error for POST with empty body', async () => {
        const { body } = await request
          .post(url)
          .set('X-PL-Signature', 'signature')
          .expect(403);

        expect(body.errorCode).toEqual(
          PartnerRequestErrorCodes.IncorrectSignature,
        );
      });

      it('returns error for POST without timestamp', async () => {
        const { body } = await request
          .post(url)
          .set('X-PL-Signature', 'signature')
          .send({ key: 'value' })
          .expect(403);

        expect(body.errorCode).toEqual(
          PartnerRequestErrorCodes.IncorrectSignature,
        );
      });

      it('returns Timestamp expired error', async () => {
        const expiredTimestamp =
          Date.now() - testParams.signatureExpirationTimeMs - 1;
        const { body } = await request
          .post(url)
          .set('X-PL-Signature', 'signature')
          .send({ timestamp: expiredTimestamp })
          .expect(403);

        expect(body.errorCode).toEqual(
          PartnerRequestErrorCodes.OutdatedSignature,
        );
      });

      it('returns Invalid signature error', async () => {
        const timestamp = Date.now();
        const { body } = await request
          .post(url)
          .set('X-PL-Signature', 'signature')
          .send({ timestamp })
          .expect(403);

        expect(body.errorCode).toEqual(
          PartnerRequestErrorCodes.IncorrectSignature,
        );
      });
    });
  });

  describe('GET /balance', () => {
    it('return error for GET request with signature', async () => {
      await request
        .get('/balance')
        .query({ playerId: playerId })
        .set('X-PL-Signature', 'signature')
        .expect(400);
    });
  });
});
