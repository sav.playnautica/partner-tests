import {
  BetOperation,
  CustomPlayerType,
  GameType,
  PartnerBetRequestRejectedBetReason,
  PartnerRequestErrorCodes,
} from '@/types';
import {
  DATETIME_REGEX,
  getCustomPlayer,
  getPlayers,
  makeId,
} from '@/lib/utils';
import { BET_PARAMS, PartnerApi } from '@/lib/partner-api';
import { itIf, testParams } from '@/tests/test-helpers';

describe('make-bet', () => {
  const partnerApi = new PartnerApi(testParams);
  const gameType = GameType.Dices;

  const [{ id, token }] = getPlayers(1);
  let balance: number;

  let roundId: string;
  let betId: string;
  const { betAmount } = testParams;

  beforeEach(async () => {
    roundId = makeId('round');
    betId = makeId('bet');
    const { body } = await partnerApi.getPlayerBalance(token, id);
    balance = body.balance;
  });

  it('makes bet', async () => {
    const { body } = await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    const expectedBalance = balance - betAmount;
    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance: expectedBalance,
      rejectedBets: {},
    });

    const newBalance = await partnerApi.getPlayerBalance(token, id);
    expect(newBalance.body.balance).toEqual(expectedBalance);
  });

  it('makes multiple bets at once', async () => {
    const firstBetId = makeId('bet');
    const secondBetId = makeId('bet');
    const thirdBetId = makeId('bet');

    const { body } = await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [firstBetId]: {
            betId: firstBetId,
            betAmount,
          },
          [secondBetId]: {
            betId: secondBetId,
            betAmount,
          },
          [thirdBetId]: {
            betId: thirdBetId,
            betAmount,
          },
        },
      })
      .expect(200);

    const expectedBalance = balance - betAmount * 3;
    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance: expectedBalance,
      rejectedBets: {},
    });

    const newBalance = await partnerApi.getPlayerBalance(token, id);
    expect(newBalance.body.balance).toEqual(expectedBalance);
  });

  it('throws if making bet with invalid token', async () => {
    await partnerApi
      .createBet('invalid-token', {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(403);
  });

  it('throws if making bet for non-existing player', async () => {
    const { body } = await partnerApi
      .createBet('invalid-token', {
        gameType,
        playerId: 'invalid-player-id',
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(404);

    expect(body.errorCode).toEqual(PartnerRequestErrorCodes.PlayerNotFound);
  });

  it('throws if making bet with invalid bet amount', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount: 'this-is-not-amount' as unknown as number,
          },
        },
      })
      .expect(400);
  });

  it('returns multistatus if bet amount is larger than balance', async () => {
    const { body } = await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount: balance + 100,
          },
        },
      })
      .expect(207);

    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance: balance,
      rejectedBets: {
        [betId]: {
          reason: PartnerBetRequestRejectedBetReason.NotEnoughMoney,
        },
      },
    });
  });

  it('keeps same balance if making the same bet two or more times', async () => {
    await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    const { body } = await partnerApi
      .createBet(token, {
        gameType,
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
          },
        },
      })
      .expect(200);

    const expectedBalance = balance - betAmount;
    expect(body).toEqual({
      operationId: expect.stringMatching(/^.*$/),
      acceptedAt: expect.stringMatching(DATETIME_REGEX),
      balance: expectedBalance,
      rejectedBets: {},
    });

    const playerBalance = await partnerApi
      .getPlayerBalance(token, id)
      .expect(200);
    expect(playerBalance.body.balance).toEqual(expectedBalance);
  });

  it('throws error on unknown gameType', async () => {
    const { body } = await partnerApi
      .createBet(token, {
        gameType: 'unknown-game-type',
        playerId: id,
        roundId,
        betOperation: BetOperation.Make,
        bets: {
          [betId]: {
            betId,
            betAmount,
            // Manually set meta for unknown game type
            meta: BET_PARAMS[gameType].num_1,
          },
        },
      })
      .expect(400);

    expect(body.errorCode).toEqual(PartnerRequestErrorCodes.UnknownGameType);
  });

  itIf(
    'throws error if player banned',
    { customPlayerType: CustomPlayerType.BannedPlayer },
    async () => {
      const { id, token } = getCustomPlayer(CustomPlayerType.BannedPlayer);
      const { body } = await partnerApi
        .createBet(token, {
          gameType,
          playerId: id,
          roundId,
          betOperation: BetOperation.Make,
          bets: {
            [betId]: {
              betId,
              betAmount,
            },
          },
        })
        .expect(403);

      expect(body.errorCode).toEqual(PartnerRequestErrorCodes.PlayerBanned);
    },
  );
});
