export enum PartnerRequestErrorCodes {
  PlayerNotFound = 'player_not_found',
  PlayerBanned = 'player_banned',
  RoundNotFound = 'round_not_found',
  UnknownGameType = 'unknown_game_type',
  IncorrectSignature = 'incorrect_signature',
  OutdatedSignature = 'outdated_signature',
  TokenMissing = 'token_missing',
  IncorrectToken = 'incorrect_token',
  TooManyRequests = 'too_many_requests',
}

export enum PartnerBetRequestRejectedBetReason {
  // You can return it for any internal reason for cancelling the bet
  BetRejected = 'bet_rejected',
  NotEnoughMoney = 'not_enough_money',
  // Deprecated in v1.1, works only for bet cancellation
  BetAlreadyPaidOut = 'bet_already_paid_out',
  FreebetExpired = 'freebet_expired',
  IncorrectFreebetAmount = 'incorrect_freebet_amount',
  FreebetAlreadyUsed = 'freebet_already_used',
}

export enum PartnerRevokeBetRequestRejectedBetReason {
  BetRejected = 'bet_rejected',
  PlayerNotFound = 'player_not_found',
  BetAlreadyPaidOut = 'bet_already_paid_out',
  PlayerBanned = 'player_banned',
}

export enum PartnerPayoutRequestRejectedBetReason {
  BetNotFound = 'bet_not_found',
  PlayerNotFound = 'player_not_found',
  PlayerBanned = 'player_banned',
  // Deprecated
  BetWasCancelled = 'bet_was_cancelled',
}

export enum GameType {
  Dices = 'game_dices',
}

export enum BetOperation {
  Make = 'make',
  Cancel = 'cancel',
}

export enum PayoutSource {
  RoundFinished = 'round_finished',
  RoundCancelled = 'round_cancelled',
  RoundResultChanged = 'round_result_changed',
  RoundRolledBack = 'round_rolled_back',
}

export type MetaBet = {
  gameType: string;
  slotType: string;
  slotFactor: number;
  slotValues: string[];
};

export type MetaPayout = {
  gameType: string;
  roundResult?: string;
};

export type BetConfig = {
  betId: string;
  betAmount: number;
  freebetAmount?: number;
  freebetId?: string;
  slotType?: string;
  slotFactor?: number;
  slotValues?: string[];
  meta?: MetaBet;
};

export type BetRequest = {
  gameType: string;
  betOperation: BetOperation;
  roundId: string;
  playerId: string;
  // Injected by signature generator
  timestamp?: number;
  bets: Record<string, BetConfig>;
};

export type BetPayout = {
  betId: string;
  playerId: string;
  amount: number;
  totalAmount: number;
};

export type PayoutRequest = {
  gameType: string;
  roundId: string;
  payoutId: string;
  payoutSource: PayoutSource;
  // Injected by signature generator
  timestamp?: number;
  meta?: MetaPayout;
  roundResult?: string;
  betPayouts: Record<string, BetPayout>;
};

export type RevokeBet = {
  betId: string;
  betAmount: number;
  playerId: string;
};

export type RevokeBetRequest = {
  gameType: string;
  roundId: string;
  bets: Record<string, RevokeBet>;
  // Injected by signature generator
  timestamp?: number;
};

export type FreebetData = {
  id: string;
  amount: number;
  expiresAt: string;
};

export type TestConfig = {
  params: TestParams;
  needs: TestNeeds;
  players: Array<TestPlayer>;
  customPlayers?: Partial<TestCustomPlayers>;
};

export type TestParams = {
  apiVersion: PartnerAPIVersions;
  // Include tests for extra versions
  apiVersionIncludeTests?: PartnerAPIVersions[];
  partnerId: string;
  signatureSecret: string;
  signatureExpirationTimeMs: number;
  betAmount: number;
  betCount: number;
  payoutAmount: number;
  serverUrl: string;
  concurrency: Concurrency;
  currencyCode: string;
  jwt?: JWTConfig;
};

export type JWTConfig = {
  partnerId: string;
  secret: string;
  expiresIn: string;
};

export type Concurrency = {
  iterations: number;
  totalPlayers: number;
  groupsCount: number;
};

export type TestNeeds = {
  playerCount: number;
  playerMinBalance: number;
};

export enum CustomPlayerType {
  BannedPlayer = 'bannedPlayer',
  TestingPlayer = 'testingPlayer',
  FreebetPlayer = 'freebetPlayer',
}

export enum FreebetTestType {
  NewFreebet = 'newFreebet',
  RegularFreebet = 'regularFreebet',
  IncorectAmountFreebet = 'incorrectAmountFreebet',
  ExpiredFreebet = 'expiredFreebet',
  AlreadyUsedFreebet = 'alreadyUsedFreebet',
  ConcurrentlyUsedFreebet = 'concurrentlyUsedFreebet',
  RevokedFreebet = 'revokedFreebet',
}

export type CustomPlayerPayload = {
  [CustomPlayerType.BannedPlayer]: undefined;
  [CustomPlayerType.TestingPlayer]: undefined;
  [CustomPlayerType.FreebetPlayer]: {
    freebetTypes: Record<FreebetTestType, string>;
  };
};

export type TestCustomPlayers = Record<CustomPlayerType, TestPlayer>;

export type TestPlayer = {
  id: string;
  token: string;
  currency: string;
};

export type CreateTestPlayerAdminResponce = TestPlayer & {
  balance: number;
  isTest?: boolean;
  isBanned?: boolean;
  freebets?: Array<FreebetData>;
};

export type CustomPlayer<T extends CustomPlayerType> = TestPlayer & {
  payload: CustomPlayerPayload[T];
};

export enum PartnerAPIVersions {
  V1_0 = '1.0',
  V1_1 = '1.1',
}
