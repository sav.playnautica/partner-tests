import fs from 'fs';
import yaml from 'yaml';
import dotenv from 'dotenv';
import {
  CustomPlayer,
  CustomPlayerType,
  FreebetTestType,
  PartnerAPIVersions,
  TestConfig,
  TestNeeds,
  TestParams,
} from '@/types';
import { configPath } from '@/lib/config';
import { PartnerApi } from '@/lib/partner-api';
import { dateOffsetSec } from '@/lib/utils';
import _ from 'lodash';

dotenv.config();

(async () => {
  const serverUrl =
    process.env.PARTNER_URL || 'http://localhost:3000/api/partner';
  const signatureSecret = process.env.PARTNER_SIGNATURE_SECRET || 'MY_SECRET';
  const signatureExpirationTimeMs = Number(
    process.env.PARTNER_SIGNATURE_EXPIRATION_TIME_MS || '30000',
  );
  const currencyCode = process.env.PARTNER_DEFAULT_CURRENCY_CODE || 'RUB';

  console.log(`Regenerating config: ${configPath}`);
  console.log(`Server URL: ${serverUrl}`);
  console.log(
    `Signature: secret "${signatureSecret}", expiration time: ${signatureExpirationTimeMs} ms`,
  );

  const testParams: TestParams = {
    apiVersion: PartnerAPIVersions.V1_1,
    apiVersionIncludeTests: [PartnerAPIVersions.V1_0],
    partnerId: 'dummy_partner',
    betAmount: 100,
    betCount: 10,
    payoutAmount: 200,
    serverUrl,
    signatureSecret,
    currencyCode,
    signatureExpirationTimeMs,
    concurrency: {
      iterations: 1,
      groupsCount: 10,
      totalPlayers: 30,
    },
  };

  const testNeeds: TestNeeds = {
    playerCount: 30,
    playerMinBalance: 100000,
  };

  const partnerApi = new PartnerApi(testParams);
  if (!process.env.PLAYNAUTICA_API_TOKEN) {
    throw new Error('PLAYNAUTICA_API_TOKEN is not defined');
  }
  const players = await partnerApi.adminCreatePlayers(
    'player-id-#',
    'valid-token-#',
    testNeeds.playerMinBalance,
    { from: 1, to: testNeeds.playerCount },
  );
  const bannedPlayer = await partnerApi.adminGeneratePlayer({ isTest: false });
  await partnerApi.adminBanPlayer(bannedPlayer);
  const testingPlayer = await partnerApi.adminGeneratePlayer({ isTest: true });
  const freebetPlayer = await generatePlayerWithFreebets(partnerApi);

  const testConfig: TestConfig = {
    params: testParams,
    needs: testNeeds,
    customPlayers: {
      bannedPlayer: _.pick(bannedPlayer, 'id', 'token', 'currency'),
      testingPlayer: _.pick(testingPlayer, 'id', 'token', 'currency'),
      freebetPlayer,
    },
    players,
  };
  fs.writeFileSync(configPath, yaml.stringify(testConfig), 'utf-8');
})().catch(err => {
  console.error(err);
  process.exit(1);
});

async function generatePlayerWithFreebets(
  partnerApi: PartnerApi,
): Promise<CustomPlayer<CustomPlayerType.FreebetPlayer>> {
  const freebetsInputs = Object.values(FreebetTestType).map(type => {
    const expiresAt =
      type === FreebetTestType.ExpiredFreebet
        ? dateOffsetSec(new Date(), -360000)
        : dateOffsetSec(new Date(), +360000);
    return {
      id: `freebet:${type}`,
      amount: 100000,
      expiresAt,
    };
  });

  const freebetPlayer = await partnerApi.adminGeneratePlayer({
    freebets: freebetsInputs,
  });

  return {
    ..._.pick(freebetPlayer, 'id', 'token', 'currency'),
    payload: {
      freebetTypes: Object.values(FreebetTestType).reduce((acc, type) => {
        acc[type] = `freebet:${type}`;
        return acc;
      }, {} as Record<FreebetTestType, string>),
    },
  };
}
